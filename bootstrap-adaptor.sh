#!/bin/bash

_arch=$(uname -s)

if [ ${_arch} == "Linux" ]; then
	_cwd=`readlink -f "$( dirname "$0" )"`
elif [ ${_arch} == "Darwin" ]; then
	__cwd="import os, sys; print os.path.realpath(\"$( dirname $0 )\")"
	_cwd=`python -c "${__cwd}"`
fi


${_cwd}/sbin/tls-adaptor.py $@
exit $?
