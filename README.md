# SPECS Enforcement -- HAProxy #
SPECS Enforcement HAProxy is in charge with the configuration of the PROXY Terminator for secured channels communication.

# Table of contents #
[TOC]

## Installation ##

### Requirements ###

* haproxy >= 1.5.14
* systemd (for process management)

### Installation steps ###

These installation steps are compatible with mOS 4.0.x or OpenSUSE 13.1 (with SPECS repositories activated) environments.

* Install the requirements

```
#!bash

zypper install haproxy
cd /opt
hg clone https://bitbucket.org/specs-team/specs-mechanism-enforcement-tls
```

### Usage ###

* TLS Configurator

```
#!bash

# use the appropiate options to generate PROXY Terminator configuration file
/opt/specs-mechanism-enforcement-tls/bin/tls-configurator --help

	--m3      - enforce M3 Crypto Strength level < 7
	--m3-plus - enforce M3 Crypto Strength level >= 7
	--m4      - enforce M4 Forward Secrecy
	--m5      - enforce M5 HTTP Strict Transport Security
	--m6      - enforce M6 HTTP to HTTPS Redirection
	--m7      - enforce M7 Secure Cookies Forced
	--m10     - enforce M10 Certificate Pinning

	--tls-backend {address}:{port} - TLS Backend connection
	--clean   - remove configuration file and SSL certificate chain
```

* TLS Prober

```
#!bash

/opt/specs-mechanism-enforcement-tls/bin/tls-prober --help

	--m3        - check M3 Crypto Strength level < 7
	--m3-plus   - check M3 Crypto Strength level >= 7
	--m4        - check M4 Forward Secrecy
	--m5        - check M5 HTTP Strict Transport Security
	--m6        - check M6 HTTP to HTTPS Redirection
	--m7        - check M7 Secure Cookies Forced
	--m10       - check M10 Certificate Pinning
	--tls-msr7  - check TLS Terminator availability
	--tls-msr8  - check TLS Backend availability
```

* TLS Controller

```
#!bash

# use control flags to manage the PROXY Terminator
/opt/specs-enforcement-haproxy/bin/tls-controller [start|stop|restart|status]
```

Very important:

* PROXY Terminator uses a self-signed certificate for TLS communication (current version doesn't support custom SSL certificates);

# NOTICE #

This product includes software developed at "Institute e-Austria, Timisoara", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).

* http://www.specs-project.eu/
* http://www.ieat.ro/

*Developers:*

    Silviu Panica, silviu@solsys.ro / silviu.panica@e-uvt.ro

*Copyright:*

```
Copyright 2013-2015, Institute e-Austria, Timisoara, Romania
    http://www.ieat.ro/

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```