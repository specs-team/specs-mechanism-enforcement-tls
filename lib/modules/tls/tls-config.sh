#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/

# TLS related to enforcement
_tls_metric_m3=tls-enforce-M3.conf
_tls_metric_m3_plus=tls-enforce-M3_plus.conf
_tls_metric_m4=tls-enforce-M4.conf
_tls_metric_m5=tls-enforce-M5.conf
_tls_metric_m6=tls-enforce-M6.conf
_tls_metric_m7=tls-enforce-M7.conf
_tls_metric_m10=tls-enforce-M10.conf
####