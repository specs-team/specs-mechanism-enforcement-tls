#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

_banner="[tls-prober]"

## metric variables (do not edit)
metric_m3=0
metric_m3_plus=0
metric_m4=0
metric_m5=0
metric_m6=0
metric_m7=0
metric_m10=0
tls_msr7=0
tls_msr8=0
####

_proxy_prober_monitoring_component="tls-prober"
_proxy_prober_monitoring_object="tls-prober_object"
_proxy_prober_monitoring_labels=("tls-prober" "tls-metrics")
_proxy_prober_monitoring_type="alert"
_proxy_prober_monitoring_data=("metric_m3=6")


################################

function tls_prober_arguments_parser() {
	_nargs=$#
	ARGS=$(getopt -o a,b,c,d,e,f,g,h,i,j --longoptions 'm3,m3-plus,m4,m5,m6,m7,m10,help,tls-msr7,tls-msr8' -n "proxy-configurator.sh" -- "$@");
	if [[ $? -ne 0 && $# -ne 0 ]];then
		tls_prober_helper
		exit 1
	fi

	eval set -- "${ARGS}"

	while true ; do
	    case "$1" in
	    	--m3) metric_m3=1; shift;
	        ;;
	        --m3-plus) metric_m3_plus=1; shift;
	        ;;
	    	--m4) metric_m4=1; shift;
	        ;;
	        --m5) metric_m5=1; shift;
	        ;;
	        --m6) metric_m6=1; shift;
	        ;;
	        --m7) metric_m7=1; shift;
	        ;;
	        --m10) metric_m10=1; shift;
	        ;;
	        --tls-msr7) tls_msr7=1; shift;
	        ;;
	        --tls-msr8) tls_msr8=1; shift;
	        ;;
	        --help) tls_prober_helper; exit 0;
	        ;;
	        --) shift; break;
	        ;;
	    esac
	done

	# if no parameter set then check the cache folder
	if [ ${_nargs} -eq 0 ];then
		if [ ! -d ${_proxy_prober_cache} ];then
			mkdir -p ${_proxy_prober_cache}
		fi
		_msr_list=$(ls ${_proxy_prober_cache})
		if [ ! -z ${_msr_list} ];then
			__m=(${_msr_list})
			if [ ${#__m{@}} -eq 0 ];then
				echo "${_banner} No measurements found in (${_proxy_prober_cache})."
				exit 1
			fi
			for ((i=0; i<${#__m[@]}; i++)) {
				case "${__m[i]}" in
					m3) metric_m3=1; shift;
			        ;;
			        m3-plus) metric_m3_plus=1; shift;
			        ;;
			    	m4) metric_m4=1; shift;
			        ;;
			        m5) metric_m5=1; shift;
			        ;;
			        m6) metric_m6=1; shift;
			        ;;
			        m7) metric_m7=1; shift;
			        ;;
			        m10) metric_m10=1; shift;
			        ;;
			        tls-msr7) tls_msr7=1; shift;
			        ;;
			        tls-msr8) tls_msr8=1; shift;
			        ;;
				esac
			}
		else
			echo "${_banner} No measurements were configured to probe!"
			exit 1
		fi
	fi
	tls_prober_check_metrics
}


function tls_prober_helper() {
	echo
	echo "   Usage for TLS Prober module: $0 [options]"
	echo
	echo "      Options:"
	echo
	echo "            --m3        - check M3 Crypto Strength level < 7"
	echo "            --m3-plus   - check M3 Crypto Strength level >= 7"
	echo "            --m4        - check M4 Forward Secrecy"
	echo "            --m5        - check M5 HTTP Strict Transport Security"
	echo "            --m6        - check M6 HTTP to HTTPS Redirection"
	echo "            --m7        - check M7 Secure Cookies Forced"
	echo "            --m10       - check M10 Certificate Pinning"
	echo "            --tls-msr7  - check TLS Terminator availability"
	echo "            --tls-msr8  - check TLS Backend availability"
	echo
}

function tls_prober_metric_cipher() {
	# M4 Forward secrecy
	# M3_plus Crypto Strength > 7
	# M3 Crypto Strength < 7
	if [ $# -ne 1 ];then
		echo "${_banner}:: missing metric name!"
		exit 1
	fi

	eval _t=\$metric_${1,,}
	eval _tls_f=\$_tls_metric_${1,,}

	if grep -q "`cat ${_proxy_configurator_templates}/${_tls_f}`" ${_proxy_terminator_config};then
		echo "${_banner} [OK] Enforce ${1} configuration already exists and matches the initial configuration"
		return 0
	else
		echo "${_banner} [ERROR] Enforce ${1} not met anymore. Reporting!"
		return 1
	fi

}

function tls_prober_metric_m5() {
	# HTTP Strict Transport Security
	local _head='frontend https-in'
	local _tail='##--end-frontend-https-in'
	local _mhead='#~tls-metric-m5'
	local _mtail='#~tls-metric-m5-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m5}
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner} [OK] Enforce M5 configuration already exists and matches the initial configuration"
			return 0
		else
			echo "${_banner} [ERROR] Enforce M5 not met anymore. Reporting"
			return 1
		fi
	else
		echo "${_banner} [ERROR] Metric M5 not configured to be enforced. Reporting"
		return 1
	fi
}

function tls_prober_metric_m6() {
	# HTTP Strict Transport Security
	local _head='frontend http-in'
	local _tail='##--end-frontend-http-in'
	local _mhead='#~tls-metric-m6'
	local _mtail='#~tls-metric-m6-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m6}
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner} [OK] Enforce M6 configuration already exists and matches the initial configuration"
			return 0
		else
			echo "${_banner} [ERROR] Enforce M6 not met anymore. Reporting"
			return 1
		fi
	else
		echo "${_banner} [ERROR] Metric M6 not configured to be enforced. Reporting"
		return 1
	fi
}

function tls_prober_metric_m7() {
	# HTTP Strict Transport Security
	local _head='frontend https-in'
	local _tail='##--end-frontend-https-in'
	local _mhead='#~tls-metric-m7'
	local _mtail='#~tls-metric-m7-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_M7}
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner} [OK] Enforce M7 configuration already exists and matches the initial configuration"
			return 0
		else
			echo "${_banner} [ERROR] Enforce M7 not met anymore. Reporting"
			return 1
		fi
	else
		echo "${_banner} [ERROR] Metric M7 not configured to be enforced. Reporting"
		return 1
	fi
}

function tls_prober_metric_m10() {
	# HTTP Strict Transport Security
	local _head='frontend https-in'
	local _tail='##--end-frontend-https-in'
	local _mhead='#~tls-metric-m10'
	local _mtail='#~tls-metric-m10-end'
	local _file=${_proxy_configurator_templates}/${_tls_metric_m10}
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file} ${_tls_terminator_cert};then
			echo "${_banner} [OK] Enforce M10 configuration already exists and matches the initial configuration"
			return 0
		else
			echo "${_banner} [ERROR] Enforce M10 not met anymore. Reporting"
			return 1
		fi
	else
		echo "${_banner} [ERROR] Metric M10 not configured to be enforced. Reporting"
		return 1
	fi
}

function tls_prober_metric_tls_msr7() {
	# TLS terminator availability
	. ${_proxy_home}/lib/lib-controller.sh
	if proxy_terminator_check;then
		echo "${_banner} [OK] Measurement MSR7."
		return 0
	else
		echo "${_banner} [ERROR] Measurement MSR7 failed. Reporting!"
		return 1
	fi
}

function tls_prober_metric_tls_msr8() {
	# TLS Backend availability

	local _backends=(`cat ${_proxy_terminator_config} | grep 'server proxy-backend-instance' | tr -s " " ";" | cut -d";" -f3`)
	if [ ${#_backends[@]} -eq 1 ];then
		local _remote=${_backends[0]}
		if proxy_check_endpoint_availability ${_remote};then
			echo "${_banner} [OK] Measurement MSR8."
			return 0
		else
			return 1
		fi
	else
		echo "${_banner} [ALERT] multiple endpoints detected. Use WebPool capability to check the availability"
		return 0
	fi
	echo "${_banner} [ERROR] Measurement MSR8 failed. Reporting!"
	return 1

}

function tls_prober_check_metrics() {
	if [ ${metric_m3} -eq 1 ];then
		if ! tls_prober_metric_cipher M3;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${metric_m4} -eq 1 ];then
		if ! tls_prober_metric_cipher M4;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${metric_m3_plus} -eq 1 ];then
		#
	  if ! tls_prober_metric_cipher M3_plus;then
		#	_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
		#												${_proxy_prober_monitoring_object} \
		#												_proxy_prober_monitoring_labels[@] \
		#												${_proxy_prober_monitoring_type} \
		#												_proxy_prober_monitoring_data[@]`
		#	proxy_prober_push_notification ${_monitoring_msg}
		#	rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	########
	if [ ${metric_m5} -eq 1 ];then
		if ! tls_prober_metric_m5;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${metric_m6} -eq 1 ];then
		if ! tls_prober_metric_m6;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			#
			exit 1
		fi
	fi
	if [ ${metric_m7} -eq 1 ];then
		if ! tls_prober_metric_m7;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${metric_m10} -eq 1 ];then
		if ! tls_prober_metric_m10;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${tls_msr7} -eq 1 ];then
		if ! tls_prober_metric_tls_msr7;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
	if [ ${tls_msr8} -eq 1 ];then
		if ! tls_prober_metric_tls_msr8;then
			#_monitoring_msg=`proxy_prober_push_message ${_proxy_prober_monitoring_component} \
			#											${_proxy_prober_monitoring_object} \
			#											_proxy_prober_monitoring_labels[@] \
			#											${_proxy_prober_monitoring_type} \
			#											_proxy_prober_monitoring_data[@]`
			#proxy_prober_push_notification ${_monitoring_msg}
			#rm -f ${_monitoring_msg}
			exit 1
		fi
	fi
}
