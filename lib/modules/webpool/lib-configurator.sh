#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

## metric variables (do not edit)
metric_m1=0
metric_m1_value=1
metric_m2=0
metric_m2_value=1
webpool_backend=
####

function webpool_configurator_arguments_parser() {
	ARGS=$(getopt -o a:,b:,c: --longoptions 'm1:,m2:,webpool-backend:' -n "proxy-configurator.sh" -- "$@");
	if [[ $? -ne 0 || $# -eq 0 ]];then
		webpool_configurator_helper
		exit 1
	fi
	eval set -- "${ARGS}"
	
	while true ; do
	    case "$1" in
	    	--m1) metric_m1=1; 
	    		shift;
				if [ ${#} -gt 0 ];then
	        		metric_m1_value=${1} 
	        		shift;
	        	fi
	        ;;
	    	--m2) metric_m2=1; 
	    		shift;
				if [ ${#} -gt 0 ];then
	        		metric_m2_value=${1} 
	        		shift;
	        	fi
	        ;;
	        --webpool-backend) 
	        	shift;
				if [ ${metric_m1} -eq 0 ];then
					echo "${_banner} --m1 not set. Cannot use --webpool-backend without --m1."
					webpool_configurator_helper
					exit 1
				fi
				if [ ${#} -gt 0 ];then
	        		IFS='#' read -a webpool_backend <<< "${1}"
					if [ ${#webpool_backend[@]} -ne ${metric_m1_value} ];then
						echo "${_banner} --webpool-backend value must be equal with --m1 value."
						webpool_configurator_helper
						exit 1
					fi
	        		shift;
	        	fi
	        ;;
			--clean) rm -f ${_proxy_terminator_cert}; rm -f ${_proxy_terminator_config}; shift;
	        ;;
	    	--help) webpool_configurator_helper; exit 0;
	        ;;
	        --) shift; break;
	        ;;
	    	 *) webpool_configurator_helper; exit 1;
	        ;;
	    esac
	done
}

function webpool_configurator_helper() {
	echo
	echo "   Usage for Webpool Configurator module: $0 [options]"
	echo
	echo "      Options:"
	echo
	echo "            --m1  {level_of_redundancy}              - enforce M1 Level of redundacy >= 1"
	echo "               --webpool-backend [{address1}:{port1}#{address2}:{port2}#{addressN}:{portN}]  - WebPool Backend container list"
	echo "            --m2  {level_of_diversity}               - enforce M2 level of diversity >= 1"
	echo
}

function webpool_configurator_metric_enforce_m1() {
	# Level of redundancy
	if [ $# -ne 1 ];then
		echo "${_banner}:: incorrect number of arguments!"
		exit 1
	fi
	local _head='backend default-backend'
	local _tail='##--end-backend-default'
	local _mhead='#~proxy-backend-instances'
	local _mtail='#~proxy-backend-instances-end'
	local _file=/tmp/_webpool_m1_temp.instances
	local _endpoints=("${!1}")
	
	if [ -f ${_file} ];then
		rm -f ${_file}
	fi
	echo ${_mhead} >> ${_file}
	for ((i=0; i<${#_endpoints[@]};i++));do
		if proxy_check_backend ${_endpoints[i]};then
			echo -e "server proxy-backend-instance-${i} ${_endpoints[i]} check"  >> ${_file}
		else
			rm -f ${_file}
			exit 1
		fi	
	done
	echo ${_mtail} >> ${_file}
	
	if grep -q "${_mhead}" ${_proxy_terminator_config} && grep -q "${_mtail}" ${_proxy_terminator_config};then
		if proxy_configurator_compare_content_ph ${_mhead} ${_mtail} ${_file};then
			echo "${_banner}:: the WebPool Enforce M1 configuration already exists and matches the initial configuration"
			return
		else
			proxy_configurator_replace_content_ph "${_mhead}" "${_mtail}" 0 ${_file} delete_old_entries
		fi
	else
		proxy_configurator_replace_content_ph "${_head}" "${_tail}" 2 ${_file}
	fi
	
	if [ -f ${_file} ];then
		rm -f ${_file}
	fi
}

function webpool_configurator_update() {
	if [ ${metric_m1} -eq 1 ];then
		webpool_configurator_metric_enforce_m1 webpool_backend[@]
	fi
}


