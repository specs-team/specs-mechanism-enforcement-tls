function proxy_terminator_check() {
	if [ -f "${_proxy_terminator_pid}" ];then
		if kill -0 `cat ${_proxy_terminator_pid}` 2> /dev/null;then
			return 0
		else
			return 1
		fi
	else
		return 1
	fi
}
function proxy_terminator_check_version() {
	_found=1
	for _h in ${1};do
		if [ -f "${_h}" ];then
			_h_major=`${_h} -v | grep 'HA-Proxy' | cut -d" " -f3 | cut -d"." -f1`
			_h_minor=`${_h} -v | grep 'HA-Proxy' | cut -d" " -f3 | cut -d"." -f2`
			_h_release=`${_h} -v | grep 'HA-Proxy' | cut -d" " -f3 | cut -d"." -f3`
			_supported=1
			if [ ${_h_major} -eq 1 ];then
				if [ ${_h_minor} -eq 5 ];then
					if [ ${_h_release} -gt 13 ];then
						_supported=0
					fi
				fi
			fi
			if [ ${_supported} -eq 0 ];then
				export _proxy_terminator_path=${_h}
				_found=0
			fi
		fi
		if ${found};then
			break
		fi
	done

	return ${_found}
}

function proxy_terminator_start() {
	local _banner="${_banner}:start:"
	if proxy_terminator_check;then
		echo "${_banner} PROXY Terminator already running. (`cat ${_proxy_terminator_pid}`)"
		exit 1
	else
		${_proxy_terminator_path} -f ${_proxy_terminator_config} -c -q
		if [ $? -ne 0 ];then
			echo "${_banner} PROXY Terminator configuration error!. (${_proxy_terminator_config})"
			exit 1
		fi

		${_proxy_terminator_path} -D -f ${_proxy_terminator_config} -p ${_proxy_terminator_pid}
		if proxy_terminator_check;then
			echo "${_banner} PROXY Terminator was started. (`cat ${_proxy_terminator_pid}`)"
			exit 0
		else
			echo "${_banner} PROXY Terminator failed to start."
			exit 1
		fi
	fi
}
function proxy_terminator_restart() {
	local _banner="${_banner}:restart:"
	if proxy_terminator_check;then
		kill -USR2 `cat ${_proxy_terminator_pid}`
		echo "${_banner} PROXY Terminator was restarted."
		exit 0
	else
		echo "${_banner} PROXY Terminator is not running."
		exit 1
	fi
}
function proxy_terminator_stop() {
	local _banner="${_banner}:stop:"
	if proxy_terminator_check;then
		kill -9 `cat ${_proxy_terminator_pid}`
		echo "${_banner} PROXY Terminator has stopped."
		exit 0
	else
		echo "${_banner} PROXY Terminator is already stopped."
		exit 1
	fi
}
function proxy_terminator_status() {
	local _banner="${_banner}:status:"
	if proxy_terminator_check;then
		echo "${_banner} PROXY Terminator is running. (`cat ${_proxy_terminator_pid}`)"
		exit 0
	else
		echo "${_banner} PROXY Terminator is stopped."
		exit 1
	fi
}

function _control() {
	local _banner="${_banner}:control:"
	if [ -z ${_proc_man} ];then
		echo "${_banner} no PROXY Terminator binary configured! Fatal error!"
		exit 1
	fi

	if [ $# -ne 1 ];then
		echo "${_banner} incorrent number of parameters"
		exit 1
	fi

	case "$1" in
		"start")
			if [ "${_proc_man}" == "systemd" ];then
				/usr/bin/systemctl start proxy-terminator
				return $?
			else
				proxy_terminator_start
			fi
			return $?
		;;
		"stop")
			if [ "${_proc_man}" == "systemd" ];then
				/usr/bin/systemctl stop proxy-terminator
			else
				proxy_terminator_stop
			fi
			return $?
		;;
		"restart")
			if [ "${_proc_man}" == "systemd" ];then
				/usr/bin/systemctl restart proxy-terminator
			else
				proxy_terminator_restart
			fi
			return $?
		;;
		"status")
			if [ "${_proc_man}" == "systemd" ];then
				/usr/bin/systemctl status proxy-terminator
			else
				proxy_terminator_status
			fi
			return $?
		;;
	esac

}

function _check_env() {
	if [ -f /usr/bin/systemctl1 ];then
		export _proc_man='systemd'
		if [ ! -f /etc/systemd/system/proxy-terminator.service ];then
			/usr/bin/systemctl enable ${_proxy_home}/etc/systemd/proxy-terminator.service
		fi
	else
		export _proxy_terminator_path=$(which haproxy 2> /dev/null)
		if [ -z "${_proxy_terminator_path}" ];then
			_h_paths=`find {/usr,/bin} -type f -name "haproxy" 2>/dev/null`
			if ! proxy_terminator_check_version ${_h_paths};then
				echo "${_banner}::${0}: not suitable PROXY Terminator executable found!"
				exit 1
			fi
		fi
		export _proc_man='local'
	fi
}
