#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

function proxy_check_endpoint_availability() {
	if [ $# -ne 1 ];then
		echo "${_banner}::check_endpoint_availability: incorrect number of parameters!"
		exit 1
	fi
	curl ${1} -s -f -o /dev/null --connect-timeout 3 
	if [ $? -eq 0 ];then
		return 0
	else
		return 1
	fi
}

function proxy_check_module() {
	
	if [ ! -d ${_proxy_home}/lib/modules/${1} ];then
		echo "${_banner}: module (${1}) not registered"
		exit 1
	fi
	if [ ! -f ${_proxy_home}/lib/modules/${1}/lib-configurator.sh ];then
		echo "${_banner}: module (${1}) invalid: missing lib-configurator.sh"
		exit 1
	fi
	if [ ! -f ${_proxy_home}/lib/modules/${1}/lib-prober.sh ];then
		echo "${_banner}: module (${1}) invalid: missing lib-prober.sh"
		exit 1
	fi
	if [ ! -f ${_proxy_home}/lib/modules/${1}/${1}-config.sh ];then
		echo "${_banner}: module (${1}) invalid: missing ${1}-config.sh"
		exit 1
	fi
}

function proxy_get_certificate_sha256() {
	if [ $# -ne 2 ];then
		echo "${_banner}::get_certificate_sha256: incorrect number of parameters!"
		exit 1
	fi
	local _cert_pin=$(openssl rsa -in ${1} -outform der -pubout 2>/dev/null | openssl dgst -sha256 -binary | base64)
	sed -i -e "s#%%proxy-terminator-certificate-sha256%%#${_cert_pin}#g" ${2}
}

function proxy_generate_self_signed() {
	if [ ! -f ${_proxy_terminator_cert} ];then
		openssl req \
				-x509 \
				-sha256 \
				-days 365 \
				-nodes \
				-subj '/C=EU/ST=FP7/L=SPECS-Project/OU=Cloud-Security/CN=specs-tls.clusters.i.cloud-apps.eu' \
				-newkey rsa:2048 \
				-keyout /tmp/proxy-terminator-self--key.pem \
				-out /tmp/proxy-terminator-self--cert.pem
		cat /tmp/proxy-terminator-self--key.pem /tmp/proxy-terminator-self--cert.pem > ${_proxy_terminator_cert}
		chmod 600 ${_proxy_terminator_cert}
		chown haproxy:haproxy ${_proxy_terminator_cert}
		rm -f /tmp/proxy-terminator-self--*
	fi
}

function _proxy_configurator_add_content() {
	cat ${_proxy_configurator_templates}/${1} >> ${_proxy_terminator_config}
	echo "" >> ${_proxy_terminator_config}
}

function _proxy_configurator_add_placeholder() {
	if [ $# -ne 1 ];then
		echo "${_banner}:proxy_configurator_add_placeholder: incorrect number of parameters!"
		exit 1
	fi
	echo -e "##--end-${1}\n" >> ${_proxy_terminator_config}
}

function proxy_configurator_compare_content_ph() {
	# compare content between two placeholders
	# params: head tail initial_content_file [ placeholder_cert ]
	
	if [ $# -lt 3 ];then
		echo "${_banner}:proxy_configurator_compare_content_ph: incorrect number of parameters!"
		exit 1
	fi
	
	local _head=${1}
	local _tail=${2}
	local _file=${3}
	local _ph_cert=${4}
	
	if [ -n "${_ph_cert}" ];then
		local _initial_value=$(sed -n -e "/${_head}/,/${_tail}/p" ${_file} > /tmp/_proxy_config_i && proxy_get_certificate_sha256 ${_ph_cert} /tmp/_proxy_config_i && cat /tmp/_proxy_config_i | tr -d " " | tr -d "\t" | md5sum | cut -d" " -f1 && rm -f /tmp/_proxy_config_i)
	else
		local _initial_value=$(sed -n -e "/${_head}/,/${_tail}/p" ${_file} > /tmp/_proxy_config_i && cat /tmp/_proxy_config_i | tr -d " " | tr -d "\t" | md5sum | cut -d" " -f1 && rm -f /tmp/_proxy_config_i)
	fi
	local _current_value=$(sed -n -e "/${_head}/,/${_tail}/p" ${_proxy_terminator_config} > /tmp/_proxy_config_c && cat  /tmp/_proxy_config_c | tr -d " " | tr -d "\t" | md5sum | cut -d" " -f1 && rm -f /tmp/_proxy_config_c)
	if [ "${_initial_value}" != "${_current_value}" ];then
		return 1
	fi
	return 0
}

function proxy_configurator_replace_content_ph() {
	# replace content between two placeholders
	# params: head tail current_position_increment file_to_add_lines_from [if_set_replace]

	if [ $# -lt 4 ];then
		echo "${_banner}:proxy_configurator_replace_content_ph: incorrect number of parameters!"
		exit 1
	fi
	
	local _head=${1}
	local _tail=${2}
	local _inc=${3}
	local _file=${4}
	if [ $# -eq 5 ];then
		local _ph=${5}
	fi
	local _p_head=$(cat ${_proxy_terminator_config} -n | tr -d "\t" | grep ".[0-9]${_head}$" | tr -d " " | cut -d"${_head:0:1}" -f1)
	local _p_tail=$(cat ${_proxy_terminator_config} -n | tr -d "\t" | grep ".[0-9]${_tail}" | tr -d " " | cut -d"${_tail:0:1}" -f1)
	local _ip=_p_head
	let _ip=_ip+_inc
	if [ ! -z ${_ph} ];then
		sed -i -e "${_p_head},${_p_tail}d" ${_proxy_terminator_config}
	fi
	while IFS= read -r _l;do
		sed -i -e "${_ip}i\\\t$(echo "${_l}" | sed -e 's/[\/&]/\\&/g')" ${_proxy_terminator_config}
		let _ip=_ip+1
	done < ${_file}
}

function proxy_check_backend() {
	if [ $# -lt 1 ];then
		echo "${_banner}::proxy_check_backend: incorrect number of parameters!"
		exit 1
	fi
	
	local _hostname=`echo ${1} | cut -d":" -f1`
	local _port=`echo ${1} | cut -d":" -f2`
	
	if [ -z ${_hostname} ];then
		echo "${_banner}:proxy_check_backend: backend not specified in correct format [hostname:port]"
		return 1
	fi
	if [ "${_port}" == "${_hostname}" ];then
		echo "${_banner}:proxy_check_backend: backend not specified in correct format [hostname:port]"
		return 1
	fi
	
	return 0
}

function proxy_update() {
	${_module}_configurator_update
}

function proxy_initial() {
	
	echo -n "" > ${_proxy_terminator_config}
	
	_proxy_configurator_add_content 'conf-global.conf'
	_proxy_configurator_add_content 'conf-global-default-ciphers.conf'
	_proxy_configurator_add_placeholder 'global'
	_proxy_configurator_add_content 'conf-defaults.conf'
	_proxy_configurator_add_placeholder 'defaults'
	_proxy_configurator_add_content 'conf-admin-url.conf'
	_proxy_configurator_add_placeholder 'listen-admin'
	_proxy_configurator_add_content 'frontend-http-in.conf'
	_proxy_configurator_add_content 'frontend-http-in-default-backend.conf'
	_proxy_configurator_add_placeholder 'frontend-http-in'
	_proxy_configurator_add_content 'frontend-https-in.conf'
	_proxy_configurator_add_placeholder 'frontend-https-in'
	_proxy_configurator_add_content 'backend-default.conf'
	_proxy_configurator_add_placeholder 'backend-default'
	
}

function proxy_extra_updates() {
	if grep -q '%%proxy-terminator-cert%%' ${_proxy_terminator_config};then
		sed -i -e "s#%%proxy-terminator-cert%%#${_proxy_terminator_cert}#g" ${_proxy_terminator_config}
	fi	
	if grep -q '%%proxy-terminator-admin-username%%:%%proxy-terminator-admin-password%%' ${_proxy_terminator_config};then
		sed -i 's/%%proxy-terminator-admin-username%%:%%proxy-terminator-admin-password%%/root:mebicatomema/g' ${_proxy_terminator_config}
	fi
}

### proxy prober common functions
function proxy_prober_push_message() {
	if [ $# -ne 5 ];then
		echo "${_banner}:: incorrect number of arguments!"
		exit 1
	fi
	
	local _component=${1} # string
	local _object=${2} # string
	local _labels=("${!3}") # array("string")
	local _type=${4} # string
	local _data=("${!5}") # array("key=value")
	local _file=$(mktemp)
	
	echo "{" > ${_file}
	echo "  \"component\" : \"${_component}\"," >> ${_file}
    echo "  \"object\" : \"${_object}\"," >> ${_file}
    echo "  \"labels\" : [" >> ${_file}
	if [ -n ${_labels} ];then
		for ((i=0; i<${#_labels[@]}; i++)) {
				echo -n "                \"${_labels[$i]}\"" >> ${_file}
				if [ $i -ne $((${#_labels[@]}-1)) ];then
					echo "," >> ${_file}
				else
					echo >> ${_file}
				fi
			}
	fi
	echo "             ]," >> ${_file}
    echo "  \"type\" : \"${_type}\"," >> ${_file}
    echo "  \"data\" : {" >> ${_file}
    if [ -n ${_data} ];then
		for ((i=0; i<${#_data[@]}; i++)) {
			echo -n "              \"$(echo ${_data[$i]} | cut -d"=" -f1)\" : \"$(echo ${_data[$i]} | cut -d"=" -f2)\"" >> ${_file}
			if [ $i -ne $((${#_data[@]}-1)) ];then
				echo "," >> ${_file}
			else
				echo >> ${_file}
			fi
		}
	fi
    echo "           }," >> ${_file}
    echo "  \"timestamp\" : \"$(date +%s)\"" >> ${_file}
    echo "}" >> ${_file}
    
    echo -n ${_file}
}

function proxy_prober_push_notification() {
	# args: /path/to/json/file/to/post/to/eventhub
	if [ $# -ne 1 ];then
		echo "${_banner}::proxy_prober_push_notification: incorrect number of arguments!"
		exit 1
	fi
	local _http_client_cmd=
	if [ -n ${_proxy_prober_monitoring_url} ];then
		if [[ -z ${_proxy_prober_monitoring_username} && -z ${_proxy_prober_monitoring_password} ]];then
			_http_client_cmd="curl -4 -H Content-Type:application/json -X POST --data-binary @/dev/stdin ${_proxy_prober_monitoring_url}"
		else
			_http_client_cmd="curl -4 -H Content-Type:application/json -X POST --data-binary @/dev/stdin --user ${_proxy_prober_monitoring_username}:${_proxy_prober_monitoring_password}  ${_proxy_prober_monitoring_url}"
		fi
	else
		echo "${_banner}::proxy_prober_push_notification: monitoring eventhub endpoint missing!"
		exit 1
	fi
	
	echo "cat ${1} | ${_http_client_cmd}"
}