#!/usr/bin/env python
'''
Copyright 2015, Institute e-Austria, Timisoara, Romania
    http://www.ieat.ro/
Developers:
 * Silviu Panica, silviu.panica@e-uvt.ro / silviu@solsys.ro

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at:
    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
'''

import sys, time, atexit, os, json
from signal import SIGTERM
import logging

tlsCwd = os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), '..')

### Logging configuration
tlsLogger = logging.getLogger('tls-adaptor')
hdlr = logging.FileHandler(tlsCwd + '/var/log/tls-adaptor.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
tlsLogger.addHandler(hdlr)
tlsLogger.setLevel(logging.INFO)
###
################################################################################
class TlsMeasurementsMatrix:
    proberMeasurementMatrix = '''
    {
      "tls_crypto_strength_level_msr1" : {
        "prober-args" : {
          "default" : "--m3-plus",
          "special" : "--m3"
        }
      },
      "tls_forward_secrecy_msr2" : {
        "prober-args" : {
          "default" : "--m4"
        }
      },
      "tls_hsts_msr3" : {
        "prober-args" : {
          "default" : "--m5"
        }
      },
      "tls_http_to_https_redirect_msr4" : {
        "prober-args" : {
          "default" : "--m6"
        }
      },
      "tls_force_secure_cookies_msr5" : {
        "prober-args" : {
          "default" : "--m7"
        }
      },
      "tls_certificate_pinning_msr6" : {
        "prober-args" : {
          "default" : "--m10"
        }
      },
      "tls_terminator_availability_msr7" : {
        "prober-args" : {
          "default" : "--tls-msr7"
        }
      },
      "tls_endpoint_availability_msr8" : {
        "prober-args" : {
          "default" : "--tls-msr8"
        }
      }
    }
    '''

    def __init__(self):
        try:
            self._msrMatrix = json.loads(self.proberMeasurementMatrix)
        except:
            self._msrMatrix = None

################################################################################
class TlsProberWrapper:

    def __init__(self):
        pass

    def checkMeasurement(self, measurement, value):
        import subprocess
        _mm = TlsMeasurementsMatrix()._msrMatrix
        if _mm == None:
            tlsLogger.info('TLS ERROR: Measurement Matrix incosistent.')
            return 99
        if measurement == 'tls_crypto_strength_level_msr1':
            if value < 7:
                _r = subprocess.call([tlsCwd + '/bin/tls-prober.sh', _mm[measurement]['prober-args']['special']], stdin=None, stdout=None, stderr=None, shell=False)
            else:
                _r = subprocess.call([tlsCwd + '/bin/tls-prober.sh', _mm[measurement]['prober-args']['default']], stdin=None, stdout=None, stderr=None, shell=False)
        else:
            _r = subprocess.call([tlsCwd + '/bin/tls-prober.sh', _mm[measurement]['prober-args']['default']], stdin=None, stdout=None, stderr=None, shell=False)
        return _r
################################################################################
class EventHub:

    def __init__(self):
        pass

    def generateEvent(self, object, component, data, slaId, securityMetric, remediation=0):
        _event = {}
        _event['object'] = object
        _event['component'] = component
        _event['labels'] = []
        _event['labels'].append('sla_id_' + slaId)
        _event['labels'].append('security_metric_' + securityMetric)
        if remediation == 1:
            _event['labels'].append('remediation-event')
        if type(data) is str or type(data) is unicode:
            _event['type'] = 'string'
            _event['data'] = '%s' % data
        elif type(data) is int:
            _event['type'] = 'int'
            _event['data'] = '%s' % data
        elif type(data) is bool:
            _event['type'] = 'boolean'
            _event['data'] = '%s' % data
        else:
            return None
        _event['token'] = None;
        _event['timestamp'] = int(time.time())

        return _event

    def postEvent(self, monitoring_ip, monitoring_port, event):
        import requests
        monitoring_url = 'http://'+monitoring_ip+':'+monitoring_port+'/events/tls-prober'
        try:
            _r = requests.post(monitoring_url, data=json.dumps(event))
            tlsLogger.debug('SPECS Event POSTed to EventHub: ' + monitoring_url)
            tlsLogger.debug(json.dumps(event))
            tlsLogger.debug('~~~~~~~~~')
        except:
            tlsLogger.exception('EventHub POST operation exception')
            tlsLogger.debug('~~~~~~~~~')
            tlsLogger.debug('SPECS EventHub: ' + monitoring_url)
            tlsLogger.debug('SPECS Event: ' + json.dumps(event))
            tlsLogger.debug('~~~~~~~~~')

################################################################################
class Daemon:
    banner = '[tls-adaptor]'

    def __init__(self, pid_file, std_in='/dev/null', std_out='/dev/null', std_err='/dev/null'):
        self.pid_file = pid_file
        self.std_in = std_in
        self.std_out = std_out
        self.std_err = std_err

    def daemon(self):
        try:
            _p1 = os.fork()
            if _p1 > 0:
                sys.exit(0)
        except OSError:
            sys.stderr.write(self.banner + "[daemonizer] first fork failed\n")
            sys.exit(1)

        # move from original environment
        os.chdir("/")
        os.setsid()
        os.umask(0)

        # next fork
        try:
            _p2 = os.fork()
            if _p2 > 0:
                sys.exit(0)
        except OSError:
            sys.stderr.write(self.banner + "[daemonizer] second fork failed\n")
            sys.exit(1)

        sys.stdout.flush()
        sys.stderr.flush()
        _std_out = file(self.std_out, 'a+')
        _std_err = file(self.std_err, 'a+', 0)
        _std_in = file(self.std_in, 'r')
        self.stdout = os.dup(1)
        self.stderr = os.dup(2)
        os.dup2(_std_in.fileno(), sys.stdin.fileno())
        os.dup2(_std_out.fileno(), sys.stdout.fileno())
        os.dup2(_std_err.fileno(), sys.stderr.fileno())

        atexit.register(self.remove_pid)
        pid = str(os.getpid())
        file(self.pid_file,'w+').write("%s\n" % pid)

    def remove_pid(self):
        os.remove(self.pid_file)

    def start(self):
        try:
            fd = file(self.pid_file,'r')
            pid = int(fd.read().strip())
            fd.close()
        except OSError:
            pid = None
        except IOError:
            pid = None

        if pid:
            try:
                os.kill(pid, 0)
                sys.stderr.write(self.banner + " PID file exist. Daemon is running ... (%s)\n" % pid)
                sys.exit(1)
            except OSError:
                pass
        self.daemon()
        self.run()

    def stop(self):
        try:
            fd = file(self.pid_file,'r')
            pid = int(fd.read().strip())
            fd.close()
        except:
            pid = None

        if not pid:
            sys.stderr.write(self.banner + "[daemonizer] PID file is missing\n")
            return

        try:
            while 1:
                os.kill(pid, SIGTERM)
                time.sleep(0.2)
        except OSError, err:
            err = str(err)
            if err.find("No such process") > 0:
                if os.path.exists(self.pid_file):
                    os.remove(self.pid_file)
                print self.banner + ' daemon successfully stopped.'
            else:
                print str(err)
                sys.exit(1)

    def restart(self):
        self.stop()
        self.start()

    def run(self):
        print 'TLS Adaptor started ...'

################################################################################
class TlsAdaptorCli:
    def __init__(self):
        pass

    def probeMeasurement(self, object, component, data, slaId, measurement, monitoring_core_ip, monitoring_core_port, publishEvent=0):
        _mm = TlsMeasurementsMatrix()._msrMatrix.keys()
        _e = 0
        if measurement in _mm:
            if measurement == 'tls_crypto_strength_level_msr1':
                _r = TlsProberWrapper().checkMeasurement(measurement, int(data))
                if _r == 0:
                    _e = EventHub().generateEvent('', component, int(data), slaId, measurement, publishEvent)
                    tlsLogger.info('TLS-CLI (ok) : ' + measurement + ' : ' + json.dumps(_e))
                    print 'TLS-CLI (ok) : ' + measurement + ' : ' + json.dumps(_e)
                else:
                    if int(data) >= 7:
                        _e = EventHub().generateEvent('', component, 6, slaId, measurement, publishEvent)
                        tlsLogger.info('TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e))
                        print 'TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e)
                    else:
                        _e = EventHub().generateEvent('', component, 7, slaId, measurement, publishEvent)
                        tlsLogger.info('TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e))
                        print 'TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e)
            else:
                _r = TlsProberWrapper().checkMeasurement(measurement, data)
                if _r == 0:
                    _e = EventHub().generateEvent('', component, data, slaId, measurement, publishEvent)
                    tlsLogger.info('TLS-CLI (ok) : ' + measurement + ' : ' + json.dumps(_e))
                    print 'TLS-CLI (ok) : ' + measurement + ' : ' + json.dumps(_e)
                else:
                    if data == "yes":
                        _e = EventHub().generateEvent('', component, "no", slaId, measurement, publishEvent)
                        tlsLogger.info('TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e))
                        print 'TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e)
                    else:
                        _e = EventHub().generateEvent('', component, "yes", slaId, measurement, publishEvent)
                        tlsLogger.info('TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e))
                        print 'TLS-CLI (error) : ' + measurement + ' : ' + json.dumps(_e)
            if publishEvent == 1:
                if type(_e) is dict:
                    tlsLogger.info('TLS-CLI : sending the probe to the event hub (%s:%s)' % (monitoring_core_ip ,monitoring_core_port))
                    EventHub().postEvent(monitoring_core_ip, monitoring_core_port, _e)
        else:
            tlsLogger.info('TLS-CLI ERROR: requested measurement not supported [%s]' % measurement)
            print 'TLS-CLI ERROR: requested measurement not supported [%s]' % measurement
            sys.exit(1)
################################################################################
class TlsAdaptorDaemon(Daemon):
    def run(self):
        _run = True
        _sleep_time = 10
        _data_load = False
        while _run:
            tlsLogger.info("Started ... searching for a TLS adaptor simplified plan in /tmp/tls-adaptor-simplified-plan.json")
            try:
                with open('/tmp/tls-adaptor-simplified-plan.json') as dataFile:
                    plan = json.load(dataFile)
                    _data_load = True
            except:
                tlsLogger.info("No TLS Adaptor plan found. Retrying after %s seconds." % _sleep_time)
                _data_load = False

            if _data_load:
                tlsLogger.info("Found TLS Adaptor plan for SLA ID: " + plan['sla_id'])
                _e = 0
                for msr in plan['measurements']:
                    if msr['msr_id'] in TlsMeasurementsMatrix()._msrMatrix.keys():
                        thold = msr['monitoring_event']['condition']['threshold'].split(':')
                        if len(thold) == 1:
                            _r = TlsProberWrapper().checkMeasurement(msr['msr_id'], thold[0])
                            if _r == 1:
                                _e = EventHub().generateEvent('', plan['prober_id'], 'no', plan['sla_id'], msr['msr_id'])
                                tlsLogger.info('TLS (error) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                            else:
                                _e = EventHub().generateEvent('', plan['prober_id'], 'yes', plan['sla_id'], msr['msr_id'])
                                tlsLogger.info('TLS (ok) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                        elif len(thold) == 2 and thold[0] == 'metric':
                            for slo in plan['slos']:
                                if slo['metric_id'] == thold[1] and slo['capability'] == 'TLS':
                                    if msr['metrics'][0] == 'tls_crypto_strength_m3':
                                        _r = TlsProberWrapper().checkMeasurement(msr['msr_id'], int(slo['value']))
                                        if _r == 0:
                                            _e = EventHub().generateEvent('', plan['prober_id'], int(slo['value']), plan['sla_id'], msr['msr_id'])
                                            tlsLogger.info('TLS (ok) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                                        else:
                                            if int(slo['value']) >= 7:
                                                _e = EventHub().generateEvent('', plan['prober_id'], 6, plan['sla_id'], msr['msr_id'])
                                                tlsLogger.info('TLS (error) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                                            else:
                                                _e = EventHub().generateEvent('', plan['prober_id'], 7, plan['sla_id'], msr['msr_id'])
                                                tlsLogger.info('TLS (error) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                                    else:
                                        _r = TlsProberWrapper().checkMeasurement(msr['msr_id'], slo['value'])
                                        if _r == 0:
                                            _e = EventHub().generateEvent('', plan['prober_id'], slo['value'], plan['sla_id'], msr['msr_id'])
                                            tlsLogger.info('TLS (ok) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                                        else:
                                            if slo['value'] == 'yes':
                                                _e = EventHub().generateEvent('', plan['prober_id'], 'no', plan['sla_id'], msr['msr_id'])
                                                tlsLogger.info('TLS (error) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                                            else:
                                                _e = EventHub().generateEvent('', plan['prober_id'], 'yes', plan['sla_id'], msr['msr_id'])
                                                tlsLogger.info('TLS (error) : ' + msr['msr_id'] + ' : ' + json.dumps(_e))
                        else:
                            tlsLogger.info('measurements:monitoring-event:condition:threshold not set correctly! [%s]' % msr['monitoring_event']['condition']['threshold'])
                        if type(_e) is dict:
                            tlsLogger.info('TLS : sending the probe to the event hub (%s:%s)' % (plan['monitoring_core_ip'],plan['monitoring_core_port']))
                            EventHub().postEvent(plan['monitoring_core_ip'], plan['monitoring_core_port'], _e)
                            if _sleep_time != self.stringToSeconds(msr['frequency']):
                                _sleep_time = self.stringToSeconds(msr['frequency'])
                                tlsLogger.info('TLS Prober frequency set to: %s' % _sleep_time)
                            _e = 0
                        if _e == None:
                            tlsLogger.error('TLS ERROR: generated event is null')
            time.sleep(_sleep_time)

    def generateComponentId(self, plan, component, recipe):
        for vm in plan['pools'][0]['vms']:
            for comp in vm['components']:
                if comp['component_id'] == component:
                    _cmp_id = component
                    for _ip in comp['private_ips']:
                        _cmp_id = _cmp_id + '-' + _ip
                    return _cmp_id
        return 'tls_terminator'

    def stringToSeconds(self, timeString):
        unit = ''
        value = ''
        for s in timeString:
            if not s.isdigit():
                unit = unit + s
            if s.isdigit():
                value = value + s
        tlsLogger.debug('TLS:stringToSeconds: decoded unit %s and value %s' % (unit, value))
        if unit in ['s', 'sec']:
            return int(value)
        if unit in ['m', 'min']:
            return int(value)*60
        if unit in ['h', 'hours']:
            return int(value)*3600

################################################################################
if __name__ == '__main__':
    _pidFile = tlsCwd + '/var/run/tls-adaptor.pid'
    _stdOutFile = tlsCwd + '/var/log/tls-adaptor.out'
    _stdErrFile = tlsCwd + '/var/log/tls-adaptor.err'
    daemon = TlsAdaptorDaemon(_pidFile, std_out=_stdOutFile, std_err=_stdErrFile)
    if len(sys.argv) >= 2:
        if 'start' == sys.argv[1]:
            tlsLogger.info('Starting TLS Adaptor ...')
            daemon.start()
        elif 'stop' == sys.argv[1]:
            tlsLogger.info('Stopping TLS Adaptor ...')
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        elif 'cli' == sys.argv[1]:
            if len(sys.argv) != 10:
                print '\nUsage: tls-adaptor.py cli object component data sla_id measurement monitoring_core_ip monitoring_core_port publish(0,1) \n'
            else:
                TlsAdaptorCli().probeMeasurement(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8], int(sys.argv[9]))
        else:
            print "[tls-adaptor] Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "\n[tls-adaptor] Usage: %s start|stop|restart|cli \n" % os.path.basename(sys.argv[0])
        sys.exit(2)
