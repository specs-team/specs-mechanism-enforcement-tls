#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

set -e

_arch=$(uname -s)

if [ ${_arch} == "Linux" ]; then
	_cwd=$(dirname `readlink -f "$0"`)
elif [ ${_arch} == "Darwin" ]; then
	__cwd="import os, sys; print os.path.realpath(\"$( dirname $0 )\")"
	_cwd=`python -c "${__cwd}"`
fi

if [ -f ${_cwd} ];then
	_proxy_home=$(dirname ${_cwd%/*})
else
	_home=''
	_proxy_home=${_home}/opt/specs-mechanism-enforcement-tls
fi

##
if [ ! -f ${_proxy_home}/etc/proxy-config.sh ];then
	echo "${_proxy_home} Couldn't find PROXY Enforcement configuration file (${_proxy_home}/etc/proxy-config.sh)!!"
	exit 1
fi
####

## load tools
_banner='[proxy-prober]'
_wrapper=`basename $0`
_module=`echo ${_wrapper} | cut -d"-" -f1`
. ${_proxy_home}/etc/proxy-config.sh
. ${_proxy_home}/lib/lib-default.sh
proxy_check_module ${_module}
. ${_proxy_home}/lib/modules/${_module}/lib-prober.sh
. ${_proxy_home}/lib/modules/${_module}/${_module}-config.sh
####

_nargs=$#

${_module}_prober_arguments_parser $@

exit 0
