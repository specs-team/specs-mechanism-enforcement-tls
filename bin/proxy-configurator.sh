#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

set -e

_arch=$(uname -s)

if [ ${_arch} == "Linux" ]; then
	_cwd=`readlink -f "$0"`
elif [ ${_arch} == "Darwin" ]; then
	__cwd="import os, sys; print os.path.realpath(\"$( dirname $0 )\")"
	_cwd=`python -c "${__cwd}"`
fi

if [ -f ${_cwd} ];then
	_proxy_home=$(dirname ${_cwd%/*})
else
	_home=''
	_proxy_home=${_home}/opt/specs-mechanism-enforcement-tls
fi

_banner='[proxy-configurator]'

_wrapper=`basename $0`
_module=`echo ${_wrapper} | cut -d"-" -f1`

## loading libraries
. ${_proxy_home}/etc/proxy-config.sh
. ${_proxy_home}/lib/lib-default.sh
proxy_check_module ${_module}
. ${_proxy_home}/lib/modules/${_module}/lib-configurator.sh
. ${_proxy_home}/lib/modules/${_module}/${_module}-config.sh
####

${_module}_configurator_arguments_parser $@

if [ ! -d ${_proxy_home}/etc/ssl/private ];then
	mkdir -p ${_proxy_home}/{bin,etc/ssl/private,var/lib,var/run,var/log}
fi

if [ ! -f ${_proxy_terminator_config} ];then
	proxy_generate_self_signed
	proxy_initial
	proxy_extra_updates
	echo "[proxy-configurator] Proxy Terminator configuration file generated. (${_proxy_terminator_config})"
fi

proxy_update

exit 0
