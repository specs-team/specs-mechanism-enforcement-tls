#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

_arch=$(uname -s)

if [ ${_arch} == "Linux" ]; then
	_cwd=`readlink -f "$0"`
elif [ ${_arch} == "Darwin" ]; then
	__cwd="import os, sys; print os.path.realpath(\"$( dirname $0 )\")"
	_cwd=`python -c "${__cwd}"`
fi

if [ -f ${_cwd} ];then
	_proxy_home=$(dirname ${_cwd%/*})
else
	_home=''
	_proxy_home=${_home}/opt/specs-mechanism-enforcement-tls
fi

## load proxy-config
. ${_proxy_home}/etc/proxy-config.sh
. ${_proxy_home}/lib/lib-controller.sh
_banner='[proxy-controller]'
####


_check_env

case "$1" in
	"start")
		_control start
		exit $?
	;;
	"stop")
		_control stop
		exit $?
	;;
	"status")
		_control status
		exit $?
	;;
	*)
		echo "Usage: $0 [start|stop|status]"
		exit 0
	;;
esac
