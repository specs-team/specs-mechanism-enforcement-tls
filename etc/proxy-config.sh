#!/bin/bash

# Copyright 2013-2015, Institute e-Austria, Timisoara, Romania, http://ieat.ro/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at:
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#     ----------------------------------------
#
# This product includes software developed at "Institute e-Austria, Timisoara", http://www.ieat.ro/ .
#
# Developers:
#  * Silviu Panica <silviu@solsys.ro>, <silviu.panica@e-uvt.ro>

#_proxy_home='/opt/specs-mechanism-enforcement-tls'

_proxy_terminator_config=${_proxy_home}/etc/proxy-terminator.conf
_proxy_terminator_cert=${_proxy_home}/etc/ssl/private/proxy-terminator-self.pem
_proxy_terminator_pid=${_proxy_home}/var/run/proxy-terminator.pid
_proxy_terminator_config=${_proxy_home}/etc/proxy-terminator.conf

_proxy_configurator_templates=${_proxy_home}/etc/templates/haproxy
_proxy_configurator_default_global=('conf-global.conf' 'conf-global-default-ciphers.conf')
_proxy_configurator_default_defaults=('conf-defaults.conf')
_proxy_configurator_default_admin=('conf-admin-url.conf')
_proxy_configurator_default_frontend_httpin=('frontend-http-in.conf' 'frontend-http-in-default-backend.conf')
_proxy_configurator_default_frontend_httpsin=('frontend-https-in.conf')
_proxy_configurator_default_backend=('backend-default.conf')

_proxy_prober_cache=${_proxy_home}/var/cache/prober
_proxy_prober_monitoring_url=
_proxy_prober_monitoring_username=
_proxy_prober_monitoring_password=
_proxy_prober_monitoring_component=
_proxy_prober_monitoring_object=
_proxy_prober_monitoring_labels=
_proxy_prober_monitoring_type=
_proxy_prober_monitoring_data=
